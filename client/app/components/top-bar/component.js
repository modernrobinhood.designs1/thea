export default Ember.Component.extend({
  tools: [],
  isCollapsed: false,
  currentTool: '',
  actions: {
    selectTool: function (tool) {
      this.set('currentTool', tool);
      this.selectToolAction(tool);
    },
    toggleSideBar: function () {
      let isCollapsed = this.isCollapsed;
      if (isCollapsed) {
        $('#appBody').removeClass('sidebar-icon-only');
      } else {
        $('#appBody').addClass('sidebar-icon-only');
      }
      this.set('isCollapsed', !this.isCollapsed);
    },
  },
});
