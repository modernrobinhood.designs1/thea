import $ from 'jquery';
export default Ember.Component.extend({
  placeHolder:
    'Kindly Enter Word List with new lines between them.\nExample:\none \ntwo \nthree',
  actions: {
    submitForSort: function () {
      let string = $('#unsortedList').val();
      string = string.split('\n').sort();
      string = string.toString().replaceAll(',', '\n');
      this.set('result', string);
    },
  },
});
