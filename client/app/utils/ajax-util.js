import $ from 'jquery';

export default Ember.Object.create({
  sendRequest: function (url, type, context, request) {
    let data = {
      isAppServed: 'true',
      request: JSON.stringify(request),
    };
    let options = {
      url: url,
      type: type ? type : 'GET',
      context: context,
      data: data,
    };
    return new Ember.RSVP.Promise(function (resolve, reject) {
      $.ajax(options)
        .then(function (data, successText, request) {
          resolve(data);
        })
        .catch(function (xhr, textStatus, errorThrown) {
          let error = {
            xhr: xhr,
            textStatus: textStatus,
            errorThrown: errorThrown,
          };
          reject(error);
        });
    });
  },
});
