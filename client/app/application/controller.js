import Controller from '@ember/controller';
import $ from 'jquery';

export default Ember.Controller.extend({
  controllerData: {},
  router: Ember.inject.service('-routing'),
  isLoading: true,
  isCollapsed: false,
  universalService: Ember.inject.service('universal-service'),
  actions: {
    transitionToRoute: function (route) {
      this.set('controllerData.activeTab', route.ID);
      this.router.transitionTo(route.LINK_TO, []);
    },
    goToRoot:function(){
      this.set('controllerData.activeTab', "TAB01");
      this.router.transitionTo("string-utils", []);
    }
  },
});
