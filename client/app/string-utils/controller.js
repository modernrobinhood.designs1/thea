import Controller from '@ember/controller';
import Ajax from '../utils/ajax-util';
import $ from 'jquery';

export default Ember.Controller.extend({
  stringList: '',
  tools: [],
  currentTool: '',
  customDelim:"",
  replaceFrom:"",
  replaceTo:"",
  options: {
    delim: false,
    sort: false,
    replace: false,
    showDup: false,
    removeDup: false,
  },
  actions: {
    selectTool: function (tool) {
      //Callback Action For Future purpose
    },
    submitForSort: function () {
      let string = $('#input').val().trim();
      let replace = this.get('options.replace');
      if (replace) {
        let replaceFrom = this.get("replaceFrom");
        let replaceTo = this.get("replaceTo");
        string = string.replaceAll(replaceFrom,replaceTo);
      }
      let canDelim = this.get('options.delim');
      let delim = ' ';
      if (canDelim) {
        delim = this.get("customDelim");
      }
      string = string.split(delim);
      let sort = this.get('options.sort');
      if (sort) {
        string.sort();
      }
      let removeDup = this.get('options.removeDup');
      if (removeDup) {
        string = Array.from(new Set(string));
      }
      this.set('result', string);
    },
    toggleAdmin: function (component) {
      let compVal = this.get('options.' + component);
      this.set('options.' + component, !compVal);
    },
    clear: function () {
      $('#input').val('');
      let options = this.options;
      this.set("result",null);
      Object.keys(options).forEach(function (pos) {
        options[pos] = false;
      });
      $('.pretty.p-switch input').each(function () {
        $(this).prop('checked', false);
      });
      this.set('options', options);
    },
  },
});
