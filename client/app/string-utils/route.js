import Ajax from '../utils/ajax-util';

export default Ember.Route.extend({
  setDefaultTool(self, json) {
    self.controller.set('currentTool', json.toolData[json.defaultToolIndex]);
  },
});
