package app.lycans.thea.controllers;


import app.lycans.thea.services.InfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    private InfoService infoService;

    public IndexController(InfoService infoService){
        this.infoService = infoService;
    }

    @RequestMapping("/")
    public String getIndexContent(){
        return "index";
    }
}
