package app.lycans.thea.controllers;

import app.lycans.thea.configs.ToolConfigs;
import app.lycans.thea.constants.RequestConstants;
import app.lycans.thea.constants.StringUtilsConstant;
import app.lycans.thea.constants.ToolsConstants;
import app.lycans.thea.services.StringUtilsService;
import app.lycans.thea.util.ConfigUtil;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/stringUtils")
public class StringUtilsController {
    StringUtilsService stringUtilsService;

    public StringUtilsController(StringUtilsService stringUtilsService){
        this.stringUtilsService = stringUtilsService;
    }

    @GetMapping(path = "",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,Object>> getAllStringOperations(HttpServletRequest request){
        Map<String,Object> result = new HashMap<>();
        ToolConfigs toolConfigs = ConfigUtil.getToolConfigs();
        ArrayList<Map<String,String>> toolList = toolConfigs.getToolGroup(ToolsConstants.GROUP_STRING_UTILS);
        Integer defaultToolIndex = toolConfigs.getDefaultToolOfGroup(ToolsConstants.GROUP_STRING_UTILS);
        result.put("toolData",toolList);
        result.put("defaultToolIndex",defaultToolIndex);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,Object>> serveStringUtilsRequest(HttpServletRequest request){
        Map<String,Object> result = new HashMap<>();
        String requestDataString = request.getParameter(RequestConstants.PARAM_REQUEST);
        JSONObject requestData = new JSONObject(requestDataString);
        String operation = requestData.getString("operation");
        switch (operation){
            case StringUtilsConstant.OPERATION_SPLIT_STRING:
                String sourceString = requestData.optString(StringUtilsConstant.PARAM_SOURCE_STRING,"");
                String regex = requestData.optString(StringUtilsConstant.PARAM_REGEX,"");
                result.putAll(stringUtilsService.splitString(sourceString,regex));
                break;
            case StringUtilsConstant.OPERATION_SORT_STRING:
                String stringList = requestData.optString(StringUtilsConstant.PARAM_STRING_LIST,"");
                boolean isAsc = requestData.optBoolean(StringUtilsConstant.PARAM_IS_ASC,true);
                if(!"".equals(stringList)) {
                    result.put(StringUtilsConstant.RESPONSE_SORTED_LIST,stringUtilsService.sortList(stringList,isAsc));
                }
            default:

        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
