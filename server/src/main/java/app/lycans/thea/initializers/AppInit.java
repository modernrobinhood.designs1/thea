package app.lycans.thea.initializers;



import app.lycans.thea.helper.RedisHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

@Component
public class AppInit {


    public AppInit() {
        System.out.println("MyBean instance created");
    }

    @PostConstruct
    private void init() throws IOException {
        RedisHelper.getInstance().startRedis();
        System.out.println("Verifying Resources");
    }

    @PreDestroy
    private void shutdown() throws  IOException {
        RedisHelper.getInstance().stopRedis();
        System.out.println("Shutdown All Resources");
    }

    public void close() {
        System.out.println("Closing All Resources");
    }
}