package app.lycans.thea.util;

import app.lycans.thea.configs.FilterConfigs;
import app.lycans.thea.configs.ToolConfigs;
import app.lycans.thea.constants.ConfigConstants;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

public class ConfigUtil {
    private static FilterConfigs filterConfigs= null;
    public static ToolConfigs toolConfigs = null;

    static {
        loadFilterConfigs();
    }
    private static void loadFilterConfigs() {
        filterConfigs = ConfigUtil.getConfig(FilterConfigs.class, ConfigConstants.FILTER_CONFIG);
        toolConfigs = ConfigUtil.getConfig(ToolConfigs.class,ConfigConstants.TOOL_CONFIG);
    }

    public static  <T> T getConfig(Class<T> classObject, String configName) {
        try {
            InputStream inputStream = FileUtils.getConfig(configName);
            Document document = getXmlFromFile(inputStream);
            return classObject.getDeclaredConstructor(Document.class).newInstance(document);
        } catch (IOException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Document getXmlFromFile(InputStream inputStream){
        Document document = null;
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(inputStream));
            document.getDocumentElement().normalize();
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
        return document;
    }


    public static FilterConfigs getFilterConfigs() {
        return filterConfigs;
    }

    public static ToolConfigs getToolConfigs() {
        return toolConfigs;
    }
}
