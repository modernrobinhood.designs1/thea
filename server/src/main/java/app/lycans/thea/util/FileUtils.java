package app.lycans.thea.util;



import app.lycans.thea.constants.ApplicationConstants;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

public class FileUtils {

    public static InputStream getConfig(String configName) throws IOException {
        Resource resource = new ClassPathResource(ApplicationConstants.CONFIGS+"/"+configName);
        return resource.getInputStream();
    }
}
