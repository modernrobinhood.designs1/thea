package app.lycans.thea.configs;

import app.lycans.thea.constants.NavBarConfigConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.HashMap;

public class NavBarConfigs {

    private HashMap<String,HashMap<String,Object>> navBarConfigs = new HashMap<>();

    public NavBarConfigs(Document document){
        NodeList nodes = document.getElementsByTagName("nav-bar");
        for(int i=0;i<nodes.getLength();i++){
            HashMap<String,Object> navBar = new HashMap();
            Element element =(Element) nodes.item(i);
            int displayOrder = Integer.parseInt(element.getAttribute(NavBarConfigConstants.DISPLAY_ORDER));
            String displayName = element.getAttribute(NavBarConfigConstants.DISPLAY_NAME);
            String id = element.getAttribute(NavBarConfigConstants.ID);
            String linkTo = element.getAttribute(NavBarConfigConstants.LINK_TO);
            String icon = element.getAttribute(NavBarConfigConstants.ICON);
            navBar.put(NavBarConfigConstants.DISPLAY_ORDER,displayOrder);
            navBar.put(NavBarConfigConstants.ID,id);
            navBar.put(NavBarConfigConstants.DISPLAY_NAME,displayName);
            navBar.put(NavBarConfigConstants.LINK_TO,linkTo);
            navBar.put(NavBarConfigConstants.ICON,icon);
            navBarConfigs.put(linkTo,navBar);
        }
    }

    public HashMap<String,HashMap<String,Object>> getConfigs(){
        return navBarConfigs;
    }
}
