package app.lycans.thea.configs;

import app.lycans.thea.constants.ToolsConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ToolConfigs {
    Map<String, ArrayList<Map<String,String>>> toolsGroup;
    Map<String,Integer> defaultToolMap;
    public ToolConfigs(Document document){
        this.toolsGroup = new HashMap<>();
        this.defaultToolMap = new HashMap<>();
        NodeList toolGrps = document.getElementsByTagName(ToolsConstants.TOOL_GROUP);
        for(int i=0;i<toolGrps.getLength();i++) {
            ArrayList<Map<String,String>> groupList = new ArrayList<>();
            Element toolGrp = (Element) toolGrps.item(i);
            String groupName = toolGrp.getAttribute(ToolsConstants.NAME);
            NodeList tools = toolGrp.getElementsByTagName(ToolsConstants.TOOL);
            for(int j=0;j<tools.getLength();j++){
                Map<String,String> toolMap = new HashMap<>();
                Element tool = (Element) tools.item(j);
                String name = tool.getAttribute(ToolsConstants.NAME);
                boolean isDefault = tool.hasAttribute(ToolsConstants.IS_DEFAULT) && Boolean.parseBoolean(tool.getAttribute(ToolsConstants.IS_DEFAULT));
                if(isDefault){
                    this.defaultToolMap.put(groupName,j);
                }
                toolMap.put(ToolsConstants.NAME,name);
                toolMap.put(ToolsConstants.DISPLAY_NAME,tool.getAttribute(ToolsConstants.DISPLAY_NAME));
                toolMap.put(ToolsConstants.COMPONENT,tool.getAttribute(ToolsConstants.COMPONENT));
                toolMap.put(ToolsConstants.SERVER_VALUE,tool.getAttribute(ToolsConstants.SERVER_VALUE));
                groupList.add(j,toolMap);
            }
            toolsGroup.put(groupName,groupList);
        }
    }

    public ArrayList<Map<String,String>> getToolGroup(String groupName){
        return this.toolsGroup.get(groupName);
    }
    public Integer getDefaultToolOfGroup(String groupName){
        return this.defaultToolMap.get(groupName);
    }
}
